I like tech talks. Sharing my list over the years here.  Webinars and Tutorials will also be here. Headings below will either be the talk itself or a specific topic not from a talk.

## 2020

### Fosdem - February 1 & 2

* The Hidden Early History of Unix                    https://fosdem.org/2020/schedule/event/early_unix/ 
* Do Linux Distributions Still Matter With Containers https://fosdem.org/2020/schedule/event/dldsmwc/ 
* How Containers and Kubernetes re-defined GNU/Linux  https://fosdem.org/2020/schedule/event/riek_kubernetes/

### Scale 18x - March 5 - 8

* FreeBSD: other Unix-like OS Intro                   https://youtu.be/YZn5vuOsn5o

### Foss-North - March 22, 29-32, April 1

* Understanding Building and Researching Minimal Linux Systems        https://youtu.be/FZOdbAJqDNY?t=8558

### eBPF

* Extended BPF: A New Software Type   **Brendan Gregg**  https://youtu.be/7pmXdG8-7WU  

### Docker

* Intro to Docker for CTFs  **LiveOverflow**                https://youtu.be/cPGZMt4cJ0I 
* How Docker Works - Intro to Namespaces  **LiveOverFlow**  https://youtu.be/-YnMr1lj4Z8 
* Deepdive Containers  **LiveOverflow**                     https://youtu.be/sHp0Q3rvamk 
* Docker Concepts **Engineering Man**                       https://youtu.be/6aBsjT5HoGY
* Docker Compose Supercharged **Engineering Man**           https://youtu.be/2qKlZQX1Ums

